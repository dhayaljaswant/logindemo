import React, {Component} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Dimensions,
  Text,
  // Button,
} from 'react-native';
import {connect} from 'react-redux';

import {initLogin} from '../store/actions';
import Button from '../components/Button';

const {width} = Dimensions.get('screen');

class SignInScreen extends Component {
  state = {
    email: 'eve.holt@reqres.in',
    password: 'cityslicka',
  };

  render() {
    const {email, password} = this.state;
    const {doLogin} = this.props;
    return (
      <View style={styles.main}>
        <Text style={styles.logo}>Logo</Text>
        <TextInput
          value={email}
          style={styles.input}
          placeholder="Enter email"
          onChange={value => this.setState({email: value})}
        />
        <TextInput
          secureTextEntry
          value={password}
          style={styles.input}
          placeholder="Enter Password"
          onChange={value => this.setState({password: value})}
        />
        <View style={styles.btnUpper}>
          <Button
            txt="Login"
            // disabled={!email || !password}
            onPress={() => {
              doLogin({email, password});
            }}
          />
        </View>
        {/* <Button
          style={styles.btn}
          title="Sign In"
          disabled={!email || !password}
          onPress={() => {}}
        /> */}
      </View>
    );
  }
}

// export default SignInScreen;

const mapStateToProps = state => {
  return {
    loading: state.loginReducer.loading,
    user: state.loginReducer.user || {},
    error: state.loginReducer.error || {},
    pushToken: state.loginReducer.pushToken,
  };
};

export default connect(mapStateToProps, {
  doLogin: initLogin,
})(SignInScreen);

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 200,
  },
  btnUpper: {marginHorizontal: 10},
  logo: {
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 50,
    width: 100,
    height: 100,
    textAlign: 'center',
    paddingTop: 35,
    fontSize: 20,
    marginBottom: 60,
  },
  input: {
    borderWidth: 1,
    borderColor: '#000',
    width: width - 20,
    padding: 10,
    margin: 10,
    borderRadius: 5,
    fontSize: 20,
    // flex: 1,
    // width:'100%'
  },
});
