import * as React from 'react';
import {View, ActivityIndicator} from 'react-native';

const HomeScreen = props => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <ActivityIndicator />
    </View>
  );
};

export default HomeScreen;
