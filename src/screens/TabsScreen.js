import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {View, Text} from 'react-native';

const Tab = createBottomTabNavigator();

export default function TabsScreen() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={FirstTab} />
      <Tab.Screen name="Settings" component={SecondTab} />
    </Tab.Navigator>
  );
}

function FirstTab() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>First Tab!</Text>
    </View>
  );
}

function SecondTab() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Settings!</Text>
    </View>
  );
}
