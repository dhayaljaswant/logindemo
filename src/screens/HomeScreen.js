import * as React from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {initLogout} from '../store/actions';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Tabs Page"
          onPress={() => this.props.navigation.navigate('Tabs')}
        />
        <Button
          title="Side Nav"
          onPress={() => this.props.navigation.navigate('SideNav')}
        />

        <Button title="Logout Me" onPress={() => this.props.initLogout()} />
      </View>
    );
  }
}

// export default HomeScreen;
export default connect(null, {initLogout})(HomeScreen);
