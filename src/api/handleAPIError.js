import AsyncStorage from '@react-native-community/async-storage';
import {TOAST_ERROR} from '../utils/constants';
import * as actionTypes from '../store/actionTypes';

export function* handleWithErrorCode(error, put, call) {
  if (error && error.response && error.response.status) {
    switch (error.response.status) {
      case 401: {
        //unauthorized
        yield AsyncStorage.clear();
        yield put({
          type: actionTypes.LOGIN_FAIL,
          payload: error,
        });

        yield put({
          type: actionTypes.TOAST_SHOW,
          payload: {
            message:
              'You logged in on another device, so, logged out from here.',
            toastType: TOAST_ERROR,
          },
        });

        return;
      }
      default: {
        yield put({
          type: actionTypes.TOAST_SHOW,
          payload: {
            message:
              (error &&
                error.response &&
                error.response.data &&
                error.response.data.error) ||
              error.message,
            toastType: TOAST_ERROR,
          },
        });
      }
    }
  }
}
