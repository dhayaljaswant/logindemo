export const baseURL = 'https://reqres.in/api/';
// export const baseURL = 'https://pros.reqres.in/api/';

const config = {
  baseUrl: baseURL,
  endpoint: {
    LOGIN: 'login',
  },
};

module.exports = config;
