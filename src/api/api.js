import http from 'axios';
import {Platform} from 'react-native';
import {retrieveData, ASYNC_STORAGE_KEYS} from '../utils/asyncStorage';
import config from '../api/config';

class API {
  static async baseHeaders() {
    const token = await retrieveData(ASYNC_STORAGE_KEYS.ACCESS_TOKEN);
    return {
      // 'Content-Type': 'application/json',
      locale: 'in',
      userToken: token,
      // Accept: 'application/json',
      // 'Content-Type': 'multipart/form-data',
      deviceType: Platform.OS === 'ios' ? 1 : 2,
      // deviceType: 1,
    };
  }

  static baseUrl() {
    return '';
  }

  static get(route, headers, params, timeout = 15000) {
    return this.api('get', route, headers, params, {}, timeout);
  }

  static put(route, headers, params, data, timeout) {
    return this.api('put', route, headers, params, data, timeout);
  }

  static post(route, headers, params, data, timeout) {
    return this.api('post', route, headers, params, data, timeout);
  }

  static patch(route, headers, params, data, timeout) {
    return this.api('patch', route, headers, params, data, timeout);
  }

  static delete(route, headers, params, data, timeout) {
    return this.api('delete', route, headers, params, data, timeout);
  }

  static async api(requestType, route, headers, params, data, timeout = 15000) {
    const host = API.baseUrl();
    const url = `${config.baseUrl}${host}${route}`;
    const baseHeaders = await API.baseHeaders();

    const requestConfig = {
      headers: headers ? {...baseHeaders, ...headers} : baseHeaders,
    };

    if (params) {
      requestConfig.params = params;
    }

    http.create();
    http.defaults.timeout = timeout;

    if (requestType === 'get' || requestType === 'delete') {
      return http[requestType](url, requestConfig)
        .then(response => {
          return checkValidJSON(response);
        })
        .catch(error => {
          logApiError(error);
          console.log('error for get', error);
          return Promise.reject(error);
        });
    }

    return http[requestType](url, data, requestConfig)
      .then(response => {
        return checkValidJSON(response);
      })
      .catch(error => {
        logApiError(error);
        console.log('error for post', url);
        return Promise.reject(error);
      });
  }
}

export default API;

function checkValidJSON(response) {
  if (response.data !== 'string') return response;
  return Promise.reject(response);
}

async function logApiError(error) {
  const {url, method, data, headers} = error.config || {};
  const {
    status,
    data: responseData,
    headers: responseHeaders = {},
  } = error.response || {};
  console.log(`API Error - Details are below:
  url->${url}\n
  method->${method}\n
  data->${data}\n
  headers->${JSON.stringify(headers)}\n
  status->${status}\n
  response->${JSON.stringify(responseData)}\n
  responseHeaders->${JSON.stringify(responseHeaders)}\n`);
}
