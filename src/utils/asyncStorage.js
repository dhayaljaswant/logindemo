import AsyncStorage from '@react-native-community/async-storage';

export const storeData = async (key, data) => {
  try {
    await AsyncStorage.setItem(key, data);
  } catch (error) {
    // Error saving data
  }
};

export const retrieveData = async key => {
  try {
    const data = await AsyncStorage.getItem(key);
    return data;
  } catch (error) {
    // Error retrieving data
  }
  return null;
};

export const clearData = async key => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    // Error clearing data
  }
};

export const ASYNC_STORAGE_KEYS = {
  LOGIN_DATA: 'login_data',
  ACCESS_TOKEN: 'access_token',
};
