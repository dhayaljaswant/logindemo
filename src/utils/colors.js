const COLORS = {
  transparent: 'rgba(0,0,0,0)',
  WHITE: '#fff',
  BLACK: '#000',
  text: '#212529',
  primary: '#007bff',
  success: '#28a745',
  error: '#F2565A',
  errorLight: '#FFF1E0',
  blackColorWithHalfOpacity: 'rgba(0,0,0,0.50)',
  loaderColor: '#00ffbf',
  DISABLED: 'rgb(230,230,230)',

  COLOR_SCREEN_BG_GREEN: 'rgb(35,207,95)',

  ORANGE: 'rgb(242,150,60)',
  COLOR_INPUT_TEXT_BASE: 'rgb(201,201,201)',
  COLOR_BUTTON_BLUE: '#1BAFE2',
  COLOR_TEXT_GREY: 'rgb(140,145,151)',
  TEXT: 'rgb(64,72,82)',
  SHADOW_COLOR: 'rgba(24,139,246,0.1)',

  MAKO: '#404852',
  NEW_BORDER: 'rgba(141, 146, 151, 0.6)',
};

export default COLORS;
