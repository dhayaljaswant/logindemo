import React, {PureComponent} from 'react';
import {View, ActivityIndicator} from 'react-native';
import PropTypes from 'prop-types';
import styles from './AppLoaderStyle';
import COLORS from '../../utils/colors';
import {connect} from 'react-redux';

class AppLoader extends PureComponent {
  render() {
    const {isLoading, size, color, forceLoading = false} = this.props;
    return isLoading || forceLoading ? (
      <View style={styles.container}>
        <ActivityIndicator size={size} color={color} />
      </View>
    ) : null;
  }
}

AppLoader.propTypes = {
  visible: PropTypes.bool.isRequired,
  size: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.oneOf(['small', 'large']),
  ]),
  color: PropTypes.string,
};

AppLoader.defaultProps = {
  size: 'large',
  color: COLORS.loaderColor,
  visible: false,
};

const mapStateToProps = state => {
  return {
    isLoading: state.loaderReducer.showing,
  };
};

export default connect(mapStateToProps)(AppLoader);
