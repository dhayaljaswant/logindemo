import React from 'react';
import {Modal, ActivityIndicator, View} from 'react-native';
import Colors from '../../utils/colors';
import styles from './LoaderModelStyle';

const LoaderModal = ({propsSize, visible = true}) => {
  return (
    <Modal
      visible={visible}
      animationType="fade"
      onRequestClose={() => {}}
      transparent>
      <View style={styles.containerStyle}>
        <ActivityIndicator
          size={propsSize || 'large'}
          color={Colors.loaderColor}
        />
      </View>
    </Modal>
  );
};

export default LoaderModal;
