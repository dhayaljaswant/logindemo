import {StyleSheet} from 'react-native';
import Colors from '../../utils/colors';

export default StyleSheet.create({
  loaderParent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerStyle: {
    backgroundColor: Colors.blackColorWithHalfOpacity,
    position: 'relative',
    flex: 1,
    justifyContent: 'center',
  },
});
