import {StyleSheet, Dimensions} from 'react-native';
import COLORS from '../../utils/colors';
const {height, width} = Dimensions.get('screen');
export default StyleSheet.create({
  container: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.blackColorWithHalfOpacity,
    zIndex: 1111,
    flex: 1,
    height,
    width,
  },
});
