import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import COLORS from '../utils/colors';

const Button = ({onPress, txt, disabled, borderRadius = 4}) => {
  return (
    <TouchableOpacity
      style={[
        styles.loginButton,
        {borderRadius},
        disabled && styles.disabledBg,
      ]}
      onPress={disabled ? null : onPress}>
      <Text style={styles.loginText}>{txt}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  loginButton: {
    height: 45,
    backgroundColor: COLORS.COLOR_BUTTON_BLUE,
    marginTop: 24,
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // borderRadius: 4,
  },
  loginText: {
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'white',
    width: '100%',
  },
  disabledBg: {
    backgroundColor: COLORS.DISABLED,
  },
});

export default Button;
