import React, {PureComponent} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  Platform,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import COLORS from '../utils/colors';
import {connect} from 'react-redux';
import BellIcon from '../../assets/svg/bell.svg';
import MenuIcon from '../../assets/svg/menu.svg';

class HomeHeader extends PureComponent {
  renderUnreadCount() {
    if (Platform.OS === 'ios') {
      // PushNotification.setApplicationIconBadgeNumber(
      //   this.props.unreadCount === null ? 0 : this.props.unreadCount,
      // );
    }
    if (this.props.unreadNotificationCount > 0) {
      return (
        <View style={styles.bellBadge}>
          <Text style={styles.bellCount}>
            {this.props.unreadNotificationCount}
          </Text>
        </View>
      );
    }
  }

  render() {
    const {title = 'Home', noBg, hideRight} = this.props;
    return (
      <View
        style={[
          styles.main,
          {backgroundColor: noBg ? COLORS.WHITE : COLORS.COLOR_BUTTON_BLUE},
        ]}>
        <TouchableOpacity
          onPress={() => this.props.navigation.openDrawer()}
          style={{
            marginLeft: -5,
            paddingTop: Platform.OS === 'ios' ? 0 : 5,
            paddingLeft: 20,
          }}>
          <MenuIcon fill={noBg ? COLORS.COLOR_BUTTON_BLUE : COLORS.WHITE} />
        </TouchableOpacity>
        <Text
          style={[
            styles.title,
            {color: noBg ? COLORS.COLOR_BUTTON_BLUE : COLORS.WHITE},
          ]}>
          {title}
        </Text>
        {hideRight ? (
          <View style={[styles.headerIcon, styles.rightIcon]} />
        ) : (
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('Job', {
                screen: 'Notifications',
                // params: {job: item},
              });
              // NavigatorService.navigate('notificationList');
            }}
            style={{
              paddingTop: Platform.OS === 'ios' ? 0 : 10,
              paddingRight: 10,
            }}>
            <BellIcon />
            {/* <Image
              source={require('../images/notifications.png')}
              style={[styles.headerIcon, styles.rightIcon]}
            /> */}
            {this.renderUnreadCount()}
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

// HomeHeader.propTypes = {
//   visible: PropTypes.bool.isRequired,
//   color: PropTypes.string,
// };

// HomeHeader.defaultProps = {
//   size: 'large',
//   color: COLORS.loaderColor,
// };

const mapStateToProps = state => {
  return {
    isLoading: state.loaderReducer.showing,
    unreadNotificationCount: state.notificationReducer.unreadNotificationCount,
  };
};

export default connect(mapStateToProps)(HomeHeader);

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 15,
    height: 50,
    backgroundColor: COLORS.COLOR_BUTTON_BLUE,
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    color: COLORS.MAKO,
    alignSelf: 'center',
    fontFamily: 'Lato-SemiBold',
    paddingTop: Platform.OS === 'ios' ? 0 : 5,
    marginTop: Platform.OS === 'ios' ? -5 : 0,
  },
  headerIcon: {
    resizeMode: 'contain',
    height: 22,
    width: 22,
  },
  leftIcon: {
    marginLeft: 20,
  },
  rightIcon: {
    marginRight: 20,
  },
  bellBadge: {
    right: -8,
    height: 20,
    width: 20,
    backgroundColor: 'red',
    borderRadius: 11,
    borderWidth: 1,
    borderColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -30,
  },
  bellCount: {
    height: 15,
    width: 22,
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: 12,
    textAlign: 'center',
    fontWeight: 'bold',
  },
});
