import React from 'react';
import {StyleSheet, TouchableOpacity, Text, View, Image} from 'react-native';
import COLORS from '../utils/colors';
import {goBackSafe} from '../utils/utils';

const Header = (props) => {
  const {title, rightIcons, navigation, backAction, hideBack} = props;

  return (
    <View style={styles.main}>
      {hideBack ? (
        <View style={{width: 50}} />
      ) : (
        <TouchableOpacity
          onPress={() => {
            goBackSafe(navigation);
            if (backAction) backAction();
          }}
          style={{
            marginLeft: -5,
          }}>
          <Image
            source={require('../images/back.png')}
            style={[styles.headerIcon, styles.leftIcon]}
          />
        </TouchableOpacity>
      )}

      <Text style={styles.title}>{title}</Text>

      <View style={styles.rightIconsStyle}>
        {rightIcons && rightIcons.length ? (
          rightIcons.map(({img, onPress}) => {
            return (
              <TouchableOpacity key={img} onPress={onPress}>
                <Image
                  source={img}
                  style={[styles.headerIcon, styles.rightIcon]}
                />
              </TouchableOpacity>
            );
          })
        ) : (
          <View style={{width: 50}} />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 15,
    height: 60,
    backgroundColor: COLORS.COLOR_BUTTON_BLUE,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: -20,
    minWidth: 150,
  },
  headerIcon: {
    resizeMode: 'contain',
    height: 22,
    width: 22,
  },
  leftIcon: {
    marginLeft: 20,
  },
  rightIcon: {
    marginRight: 20,
  },
  rightIconsStyle: {
    flexDirection: 'row',
  },
});

export default Header;
