import React from 'react';
import {StyleSheet, TouchableOpacity, Text, View, Image} from 'react-native';
import COLORS from '../utils/colors';
import {goBackSafe} from '../utils/utils';
import BackIcon from '../../assets/svg/back.svg';

const HeaderNew = (props) => {
  const {title, rightIcons, navigation, backAction, hideBack, noBg} = props;

  return (
    <View style={styles.main}>
      {hideBack ? (
        <View style={{width: 50}} />
      ) : (
        <TouchableOpacity
          onPress={() => {
            goBackSafe(navigation);
            if (backAction) backAction();
          }}
          style={{
            paddingHorizontal: 15,
          }}>
          <BackIcon />
        </TouchableOpacity>
      )}

      {!noBg ? <Text style={styles.title}>{title}</Text> : null}

      <View style={styles.rightIconsStyle}>
        {rightIcons && rightIcons.length ? (
          rightIcons.map(({img, onPress}) => {
            return (
              <TouchableOpacity
                style={styles.rightIcon}
                key={img}
                onPress={onPress}>
                <Image source={img} style={[styles.headerIcon]} />
              </TouchableOpacity>
            );
          })
        ) : (
          <View style={{width: 50}} />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    left: 0,
    top: 30,
    width: '100%',
    height: 50,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 15,
    height: 60,
    zIndex: 111,
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    alignSelf: 'center',
    fontWeight: 'bold',
    marginTop: -20,
    minWidth: 150,
  },
  headerIcon: {
    resizeMode: 'contain',
    height: 18,
    width: 18,
  },
  leftIcon: {
    marginLeft: 20,
  },
  rightIcon: {
    marginRight: 15,
    backgroundColor: COLORS.COLOR_BUTTON_BLUE,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    textAlign: 'center',
    height: 40,
    width: 40,
    borderRadius: 40,
  },
  rightIconsStyle: {
    flexDirection: 'row',
  },
});

export default HeaderNew;
