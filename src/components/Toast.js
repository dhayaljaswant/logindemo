import React, {useState} from 'react';
import Toast from 'react-native-easy-toast';
import {hideToast} from '../store/actions/toastAction';
import {connect} from 'react-redux';
import COLORS from '../utils/colors';
import {TOAST_ERROR} from '../utils/constants';

const ToastView = props => {
  const {message, type = TOAST_ERROR} = props;
  const [toast, setToast] = useState(null);

  message &&
    toast &&
    toast.show(message, 3000, () => {
      props.hideToast();
    });

  const backgroundColor = type === TOAST_ERROR ? COLORS.error : COLORS.BLACK;
  const color = COLORS.WHITE;

  return (
    <Toast
      ref={toast => setToast(toast)}
      style={{backgroundColor}}
      position="top"
      positionValue={100}
      fadeInDuration={100}
      fadeOutDuration={200}
      opacity={1}
      textStyle={{color}}
    />
  );
};

const mapStateToProps = state => {
  return {
    message: state.toastReducer.message,
    type: state.toastReducer.type,
  };
};
export default connect(mapStateToProps, {
  hideToast,
})(ToastView);
