import {TOAST_SHOW, TOAST_HIDE} from '../actionTypes';

const INITIAL_STATE = {
  showing: false,
  message: '',
  // type: 'error',
  type: '',
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    case TOAST_SHOW:
      return {
        ...state,
        showing: true,
        message: actions.payload.message,
        type: actions.payload.toastType || state.type,
      };
    case TOAST_HIDE:
      return {
        ...state,
        showing: false,
        message: '',
        type: '',
      };

    default:
      return state;
  }
};
