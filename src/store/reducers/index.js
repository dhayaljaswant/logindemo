import {combineReducers} from 'redux';
import loaderReducer from './loaderReducer';
import loginReducer from './loginReducer';
import toastReducer from './toastReducer';

export default combineReducers({
  loaderReducer,
  loginReducer,
  toastReducer,
});
