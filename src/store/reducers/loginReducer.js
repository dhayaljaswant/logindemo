import * as types from '../actionTypes';

const LOGIN_INITIAL_STATE = {
  loading: false,
  user: null,
  userToken: null,
  error: null,
};

// const CHANGE_PASSWORD_INITIAL_STATE = {
//   changePasswordLoading: false,
//   changePasswordSuccess: null,
//   changePasswordError: null,
// };

// const PUSH_TOKEN_INITIAL_STATE = {
//   pushToken: null,
// };

const INITIAL_STATE = {
  ...LOGIN_INITIAL_STATE,
  //   ...PUSH_TOKEN_INITIAL_STATE,
  //   ...CHANGE_PASSWORD_INITIAL_STATE,
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {

    case types.LOGIN:
      return {
        ...state,
        ...LOGIN_INITIAL_STATE,
        loading: true,
      };

    case types.LOGIN_START:
      return {
        ...state,
        ...LOGIN_INITIAL_STATE,
        loading: true,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        user: actions.payload,
        userToken: actions.payload && actions.payload.token,
        loading: false,
        error: null,
      };
    case types.LOGIN_FAIL:
      return {
        ...state,
        user: null,
        userToken: null,
        loading: false,
        error: actions.payload,
      };

    // case types.PUSH_TOKEN:
    //   return {
    //     ...state,
    //     pushToken: actions.payload,
    //   };

    // case types.CHANGE_PASSWORD_START:
    //   return {
    //     ...state,
    //     ...CHANGE_PASSWORD_INITIAL_STATE,
    //     changePasswordLoading: true,
    //   };
    // case types.CHANGE_PASSWORD_SUCCESS:
    //   return {
    //     ...state,
    //     ...CHANGE_PASSWORD_INITIAL_STATE,
    //     changePasswordSuccess: actions.payload,
    //   };
    // case types.CHANGE_PASSWORD_FAIL:
    //   return {
    //     ...state,
    //     ...CHANGE_PASSWORD_INITIAL_STATE,
    //     changePasswordError: actions.payload,
    //   };

    // case types.CHANGE_PASSWORD_RESET:
    //   return {
    //     ...state,
    //     ...CHANGE_PASSWORD_INITIAL_STATE,
    //   };
    default:
      return state;
  }
};
