import {LOADER_SHOW, LOADER_HIDE} from '../actionTypes';

const INITIAL_STATE = {
  showing: false,
};

export default (state = INITIAL_STATE, actions) => {
  switch (actions.type) {
    case LOADER_SHOW:
      return {
        ...state,
        showing: true,
      };
    case LOADER_HIDE:
      return {
        ...state,
        showing: false,
      };

    default:
      return state;
  }
};
