import {fork, all} from 'redux-saga/effects';
import loginSaga from './loginSaga';
import logoutSaga from './logoutSaga';

export default function* rootSaga() {
  yield all([fork(loginSaga)]);
  yield all([fork(logoutSaga)]);
}
