import {put, takeLatest, call} from 'redux-saga/effects';
import API from '../../api/api';
import config from '../../api/config';
import * as actionTypes from '../actionTypes';
import {storeData, ASYNC_STORAGE_KEYS} from '../../utils/asyncStorage';
import AsyncStorage from '@react-native-community/async-storage';
import {Platform} from 'react-native';
import {TOAST_ERROR, TOAST_SUCCESS} from '../../utils/constants';
import {handleWithErrorCode} from '../../api/handleAPIError';

function* loginSaga() {
  yield takeLatest(actionTypes.LOGIN, login);
  // yield takeLatest(actionTypes.LOGOUT, logout);
}

function* login(action) {
  yield put({
    type: actionTypes.LOADER_SHOW,
  });
  yield put({
    type: actionTypes.LOGIN_START,
    payload: action.payload,
  });
  try {
    let loginData = action.payload;
    // (loginData.userType = 1/2),
    const response = yield call(loginUser, loginData);

    if (response && response.data && response.data.token) {
      yield put({
        type: actionTypes.TOAST_SHOW,
        payload: {
          message: response.data.message,
          toastType: TOAST_SUCCESS,
        },
      });

      yield call(
        storeData,
        ASYNC_STORAGE_KEYS.LOGIN_DATA,
        JSON.stringify(response.data),
      );
      yield call(
        storeData,
        ASYNC_STORAGE_KEYS.ACCESS_TOKEN,
        response.data.token,
      );

      yield put({
        type: actionTypes.LOGIN_SUCCESS,
        payload: response.data,
      });
    } else {
      yield put({
        type: actionTypes.TOAST_SHOW,
        payload: {
          message: response.data.error,
          toastType: TOAST_ERROR,
        },
      });
      yield put({
        type: actionTypes.LOGIN_FAIL,
        payload: response.data,
      });
    }

    yield put({
      type: actionTypes.LOADER_HIDE,
    });
  } catch (error) {
    yield put({
      type: actionTypes.LOGIN_FAIL,
      payload: error,
    });

    yield put({
      type: actionTypes.LOADER_HIDE,
    });

    yield handleWithErrorCode(error, put);
  }
}

function loginUser(params) {
  return API.post(config.endpoint.LOGIN, null, null, params);
}

export default loginSaga;
