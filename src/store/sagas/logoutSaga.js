import {put, takeLatest, call} from 'redux-saga/effects';
import * as actionTypes from '../actionTypes';
import AsyncStorage from '@react-native-community/async-storage';

function* logoutSaga() {
  yield takeLatest(actionTypes.LOGOUT, logout);
}

function* clearApplicationData() {
  yield call(AsyncStorage.clear);
}

function* logout() {
  yield call(clearApplicationData, null);
  yield put({
    type: actionTypes.LOGIN_SUCCESS,
    payload: null,
  });
}

export default logoutSaga;
