import * as types from '../actionTypes';

export const initLogin = request => ({
  type: types.LOGIN,
  payload: request,
});

export const initLogout = () => ({
  type: types.LOGOUT,
});

export const setLoginData = data => ({
  type: types.LOGIN_SUCCESS,
  payload: data,
});

// export const setPushToken = token => ({
//   type: types.PUSH_TOKEN,
//   payload: token,
// });

// export const initChangePassword = request => ({
//   type: types.CHANGE_PASSWORD,
//   payload: request,
// });

// export const resetChangePassword = () => ({
//   type: types.CHANGE_PASSWORD_RESET,
// });

// export const updatePushToken = request => ({
//   type: types.UPDATE_USER_TOKEN,
//   payload: request,
// });
