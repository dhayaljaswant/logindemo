import * as types from '../actionTypes';

export const showToast = payload => ({
  type: types.TOAST_SHOW,
  payload: typeof payload === 'string' ? {message: payload} : payload,
});

export const hideToast = () => ({
  type: types.TOAST_HIDE,
});
