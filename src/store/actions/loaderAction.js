import * as types from '../actionTypes';

export const showLoader = data => ({
  type: types.LOADER_SHOW
});

export const hideLoader = () => ({
  type: types.LOADER_HIDE,
});
