import React, {Component} from 'react';
import {Provider} from 'react-redux';

import store from './src/store/store';

import AppNavigation from './AppNavigation';

import AppLoader from './src/components/Loader/AppLoader';
import ToastView from './src/components/Toast';

// console.disableYellowBox = true;
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigation />
        <AppLoader />
        <ToastView />
      </Provider>
    );
  }
}

export default App;
