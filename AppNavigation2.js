import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';
import TabsScreen from './src/screens/TabsScreen';
import SideNavScreen from './src/screens/SideNavScreen';

const Stack = createStackNavigator();

export default function AppNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home Page" component={HomeScreen} />
        <Stack.Screen
          name="Tabs"
          initialParams={{itemId: 42}}
          component={TabsScreen}
        />
        <Stack.Screen
          name="SideNav"
          initialParams={{itemId: 42}}
          component={SideNavScreen}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
