import * as React from 'react';
import {connect} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';
import TabsScreen from './src/screens/TabsScreen';
import SideNavScreen from './src/screens/SideNavScreen';
import SplashScreen from './src/screens/SplashScreen';
import SignInScreen from './src/screens/SignInScreen';
import {ASYNC_STORAGE_KEYS, retrieveData} from './src/utils/asyncStorage';
import {setLoginData} from './src/store/actions';

const Stack = createStackNavigator();

class AppNavigation extends React.PureComponent {
  state = {
    // userToken: null,
    // isSignout: false,
    loading: true,
  };

  async componentDidMount() {
    const loginData = await retrieveData(ASYNC_STORAGE_KEYS.LOGIN_DATA);
    try {
      this.props.setLoginData(JSON.parse(loginData));
      setTimeout(() => {
        this.setState({loading: false});
      }, 10);
    } catch (e) {
      setTimeout(() => {
        this.setState({loading: false});
      }, 10);
    }
  }

  render() {
    const {loading, isSignout} = this.state;
    const {userToken} = this.props;
    return (
      <NavigationContainer>
        {/* We haven't finished checking for the token yet */}
        {loading ? (
          <SplashScreen />
        ) : (
          <Stack.Navigator>
            {userToken == null ? (
              // No token found, user isn't signed in
              <Stack.Screen
                name="SignIn"
                component={SignInScreen}
                options={{
                  title: 'Sign in',
                  animationEnabled: false,
                  // When logging out, a pop animation feels intuitive
                  // You can remove this if you want the default 'push' animation
                  // animationTypeForReplace: isSignout ? 'pop' : 'push',
                }}
              />
            ) : (
              // User is signed in
              <>
                <Stack.Screen name="Home Page" component={HomeScreen} />
                <Stack.Screen
                  name="Tabs"
                  initialParams={{itemId: 42}}
                  component={TabsScreen}
                />
                <Stack.Screen
                  name="SideNav"
                  initialParams={{itemId: 42}}
                  component={SideNavScreen}
                />
              </>
            )}
          </Stack.Navigator>
        )}
        {/* <Stack.Navigator>
        <Stack.Screen name="Home Page" component={HomeScreen} />
        <Stack.Screen
          name="Tabs"
          initialParams={{itemId: 42}}
          component={TabsScreen}
        />
        <Stack.Screen
          name="SideNav"
          initialParams={{itemId: 42}}
          component={SideNavScreen}
        />
      </Stack.Navigator> */}
      </NavigationContainer>
    );
  }
}

// export default AppNavigation;

const mapStateToProps = state => {
  return {
    user: state.loginReducer.user,
    userToken: state.loginReducer.userToken,
  };
};
export default connect(mapStateToProps, {setLoginData})(AppNavigation);
